<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="css/scss.css">
    <title>Character Page</title>
</head>
<body>
<?php
//todo 1 : (Fait) créez au moins 4 armes différentes.
// lorsqu'un pc ou un npc est généré, il doit recevoir une arme aléatoire.

//todo 2 : (Fait) modifiez l'attaque (method) afin de prendre en compte l'utilisation de l'arme.
// lorsqu'un personnage réussit une attaque, les dégâts doivent provenir de l'arme utilisée par le personnage.
// les dégâts doivent être égal à une valeur aléatoire comprise entre les dégâts minimum et maximum de l'arme.

//todo 3 : (Fait) créez un nouvel élément (class) : les capacités magiques (que vous pouvez appeler simplement "magic")
// lorsqu'un pc ou un npc est généré, il doit recevoir une capacité magique aléatoire.

//todo 4 : (Fait) créez 2 catégories d'armes : les armes de corps à corps et les armes à distance.

//todo 5 : créez deux nouvelles catégories, autant valable pour les armes que pour la magie : offensive et défensive
// les capacités offensives permettront d'effectuer une attaque, tandis que les capacités défensives permettront d'esquiver OU de bloquer

//todo 6 : ajoutez la possibilité d'esquiver et de parer pour les personnages.
// les attaques des armes de corps à corps ne permettent pas d'esquive, mais permettent la parade si le personnage possède une arme défensive
// les armes à distance permettent l'esquive, mais pas la parade.

//todo 7 : Si un personnage possède une capacité magique offensive, l'attaque standard est remplacée par une attaque magique, ignorant la parade et l'esquive
// Si un personnage possède une capacité magique défensive, la valeur de défense de la capacité magique s'ajoute à la parade ou à l'esquive

//todo 8 : esquive et parade
// l'esquive accorde 10% de chance d'éviter une attaque par tranche de 10 points en valeur de défense.
// la parade accorde 10% de chance de riposter (le défenseur effectue une attaque contre l'attaquant, en plus de ses attaques normales) par tranche de 10 points en valeur d'attaque.

// todo : ajouter les caracteristiques restantes au armor
// todo : ajouter le random sur les caracteristiques a la creation du personnage
// todo : ajouter la caracteristique de puissance magique en fonction de l'int, puis faire en sorte que le sort utiliser augmente ou diminue en fonction de cette caracteristique
// todo important : method showDescription pour l'affichage du pc et du npc


use classes\Armor;
use classes\ArmorType;

spl_autoload_register(function ($class) {
    require __DIR__ . '/' . strtolower(str_replace('\\', DIRECTORY_SEPARATOR, $class)) . '.php';
});

function hr()
{
    echo '<hr>';
}

function br()
{
    echo '<br>';
}

function separation()
{
    echo "-------------";
}

\classes\weapon::init();
\classes\race::init();
\classes\spell::init();
Armor::init();

$rmagic = \classes\spell::getRandomMagic();
$rmagicnpc = \classes\spell::getRandomMagic();
$rweapon = \classes\weapon::getRandomWeapon();
$rweaponpc = \classes\weapon::getRandomWeapon();

$pc = new \classes\pc('Max', 1, $rweapon, \classes\race::$orc, $rmagic);
foreach (ArmorType::cases() as $armorType) {
    $pc->equip(Armor::getRandomArmor($armorType));
}


$npc = new \classes\npc('NPC', 2, $rweaponpc, \classes\race::$human, $rmagicnpc);
foreach (ArmorType::cases() as $armorType) {
    $npc->equip(Armor::getRandomArmor($armorType));
}


//Description des personnages /!\


?>
<div class="row">
    <div class="div1 col " style="width: 250px">
        <?php
        // Affichage fiche de personnage
        // PC FICHE DE PERSONNAGE ---------------------------------------------------------------
        echo 'Character name : ' . $pc->getName() . ' <br> ' . ' Race : ' . $pc->getRace()->getName();
        br();
        echo 'Race Hp : ' . $pc->getRace()->getHp();
        br();
        echo 'Current Character Hp : ' . $pc->getHp();
        br();
        echo 'Current Character Mana : ' . $pc->getMana();
        br();
        echo 'Spell : ' . $pc->getMagic()->getName();
        br();
        echo 'Weapon : ' . $pc->getWeapon()->getName() . ' (' . $pc->getWeapon()->getMindmg() . '-' . $pc->getWeapon()->getMaxdmg() . ')';
        br();
        separation();
        br();
        echo 'Attack : ' . $pc->getAttack();
        br();
        echo 'Defense : ' . $pc->getDefense();
        br();
        echo 'Dodge : ' . $pc->getRace()->getDodge() . '%';
        br();
        separation();
        br();
        echo 'Helmet : ' . $pc->getHelmet()->getName();
        br();
        echo 'ChestPlate : ' . $pc->getChestPlate()->getName();
        br();
        echo 'Gloves : ' . $pc->getGloves()->getName();
        br();
        echo 'Boots : ' . $pc->getBoots()->getName();
        br();
        echo 'Shield : ' . $pc->getShield()->getName();
        br();
        separation();
        br();
        echo 'Str : ' . $pc->getRace()->getStr();
        br();
        echo 'End : ' . $pc->getRace()->getEnd();
        br();
        echo 'Dex : ' . $pc->getRace()->getDex();
        br();
        echo 'Agi : ' . $pc->getRace()->getAgi();
        br();
        echo 'Int : ' . $pc->getRace()->getInt();
        hr();
        //var_dump($pc);
        hr();
        // NPC FICHE DE PERSONNAGE ----------------------------------------------------------
        echo 'Character name : ' . $npc->getName() . ' <br> ' . ' Race : ' . $npc->getRace()->getName();
        br();
        echo 'Race Hp : ' . $npc->getRace()->getHp();
        br();
        echo 'Current Character Hp : ' . $npc->getHp();
        br();
        echo 'Current Character Mana : ' . $npc->getMana();
        br();
        echo 'Spell : ' . $npc->getMagic()->getName();
        br();
        echo 'Weapon : ' . $npc->getWeapon()->getName() . ' (' . $npc->getWeapon()->getMindmg() . '-' . $npc->getWeapon()->getMaxdmg() . ')';
        br();
        echo 'Attack : ' . $npc->getAttack();
        br();
        echo 'Defense : ' . $npc->getDefense();
        br();
        echo 'Dodge : ' . $npc->getRace()->getDodge() . '%';
        br();
        separation();
        br();
        echo 'Helmet : ' . $npc->getHelmet()->getName();
        br();
        echo 'ChestPlate : ' . $npc->getChestPlate()->getName();
        br();
        echo 'Gloves : ' . $npc->getGloves()->getName();
        br();
        echo 'Boots : ' . $npc->getBoots()->getName();
        br();
        echo 'Shield : ' . $npc->getShield()->getName();
        br();
        separation();
        br();
        echo 'Str : ' . $npc->getRace()->getStr();
        br();
        echo 'End : ' . $npc->getRace()->getEnd();
        br();
        echo 'Dex : ' . $npc->getRace()->getDex();
        br();
        echo 'Agi : ' . $npc->getRace()->getAgi();
        br();
        echo 'Int : ' . $npc->getRace()->getInt();
        hr();
        //var_dump($npc);
        ?>
    </div>

    <div class="col">

        <?php

        // attaque & riposte jusqu'à la mort d'un des candidats
        $turn = 0;
        while ($pc->getHp() > 0 && $npc->getHp() > 0) {
            hr();
            echo 'Tour ' . ++$turn;
            hr();
            if (!$pc->CastSpell($npc)) {
                br();
                echo $pc->Attack($npc);
            }
            br();
            separation();
            br();
            if ($pc->getHp() > 0 && $npc->getHp() > 0) {
                if (!$npc->CastSpell($pc)) {
                    br();
                    echo $npc->Attack($pc);
                }
                br();
            }
            $pc->RegenerateMana();
            $npc->RegenerateMana();
        }
        // combat terminé, le vainqueur
        if ($pc->getHp() == 0) {
            $winner = $npc->getName();
        } else {
            $winner = $pc->getName();

        }
        echo '<p>Le combat est terminé! Le vainqueur est : ' . $winner . '</p>';


        // le NPC ne sera plus déterminé par le script index, mais proviendra d'une base de données où l'adversaire sera déterminé aléatoirement
        ?>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
</body>
</html>
