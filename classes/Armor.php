<?php

namespace classes;

class Armor
{
    private static ?array $armorStand = null;

    /**
     * @param ArmorType $armorType
     * @param string $name
     * @param int $defenseAugment
     * @param int $regenerationMana
     * @param int $manaAugment
     * @param int $strengthAugment
     */
    public function __construct(ArmorType $armorType, string $name, int $defenseAugment, int $regenerationMana, int $manaAugment, int $strengthAugment)
    {
        $this->armorType = $armorType;
        $this->name = $name;
        $this->defenseAugment = $defenseAugment;
        $this->regenerationManaAugment = $regenerationMana;
        $this->manaAugment = $manaAugment;
        $this->strengthAugment = $strengthAugment;
    }
    public static function init(): void
    {
        Armor::$armorStand = [
            new Armor(ArmorType::Helmet, 'Plate Helmet', 2, 0, 0, 0),
            new Armor(ArmorType::Helmet, 'Crown', 1, 1, 1, 0),
            new Armor(ArmorType::ChestPlate, 'ChestPlate', 3, 0, 0, 1),
            new Armor(ArmorType::ChestPlate, 'Cape', 1, 3, 3, 0),
            new Armor(ArmorType::Ring, 'Golden Ring', 0, 0,0,0),
            new Armor(ArmorType::Gloves, 'Gloves Plates', 2, 0, 0, 0),
            new Armor(ArmorType::Gloves, 'Magic Hand', 1, 2, 2, 0),
            new Armor(ArmorType::Boots, 'Boots Plates', 2, 0, 0, 0),
            new Armor(ArmorType::Boots, 'Sorcerer Boots', 1, 2, 2, 0),
            new Armor(ArmorType::Shield, 'LongShield', 4, 0, 0, 0),
            new Armor(ArmorType::Shield, 'Magic Shield', 2, 2, 2, 0),
        ];

    }

    private ArmorType $armorType;
    private string $name;
    private int $defenseAugment;
    private int $regenerationManaAugment;
    private int $manaAugment;
    private int $strengthAugment;
    private int $endAugment;
    private int $dexAugment;
    private int $agiAugment;
    private int $intAugment;
    private int $durability;

    public static function getRandomArmor(ArmorType $armorType): ?Armor
    {
        $validArmor = array();
        foreach (self::$armorStand as $value) {
            if ($value->getArmorType() == $armorType) {
                $validArmor[] = $value;
            }
        } if (empty($validArmor)){
            return null;
    }
        $random = array_rand($validArmor, 1);
        return $validArmor[$random];
    }

    public function applyAugments(character $character): void
    {

    }

    public function revertAugments(character $character): void
    {

    }

    /**
     * @return ArmorType
     */
    public function getArmorType(): ArmorType
    {
        return $this->armorType;
    }

    /**
     * @param ArmorType $armorType
     */
    public function setArmorType(ArmorType $armorType): void
    {
        $this->armorType = $armorType;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getDefenseAugment(): int
    {
        return $this->defenseAugment;
    }

    /**
     * @param int $defenseAugment
     */
    public function setDefenseAugment(int $defenseAugment): void
    {
        $this->defenseAugment = $defenseAugment;
    }

    /**
     * @return int
     */
    public function getRegenerationManaAugment(): int
    {
        return $this->regenerationManaAugment;
    }

    /**
     * @param int $regenerationManaAugment
     */
    public function setRegenerationManaAugment(int $regenerationManaAugment): void
    {
        $this->regenerationManaAugment = $regenerationManaAugment;
    }

    /**
     * @return int
     */
    public function getManaAugment(): int
    {
        return $this->manaAugment;
    }

    /**
     * @param int $manaAugment
     */
    public function setManaAugment(int $manaAugment): void
    {
        $this->manaAugment = $manaAugment;
    }

    /**
     * @return int
     */
    public function getStrengthAugment(): int
    {
        return $this->strengthAugment;
    }

    /**
     * @param int $strengthAugment
     */
    public function setStrengthAugment(int $strengthAugment): void
    {
        $this->strengthAugment = $strengthAugment;
    }

    /**
     * @return array|null
     */
    public static function getArmorStand(): ?array
    {
        return self::$armorStand;
    }

}