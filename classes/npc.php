<?php

namespace classes;

use classes\character;

class npc extends character
{
    private int $killed;

    public function setKilledScore (int $killed): void
    {
        $this->killed = $killed + 1;
    }
    /**
     * @return int
     */
    public function getKilled(): int
    {
        return $this->killed;
    }

    /**
     * @param int $killed
     */
    public function setKilled(int $killed): void
    {
        $this->killed = $killed;
    }
    //todo : ajoutez une propriété privée "killed", avec son getter et son setter

}

