<?php

namespace classes;


class fireball extends spell
{

    public string $name = "Fireball";
    public int $damage = 15;
    public string $effect = "Burn";

    /**
     * @param int $damage
     * @param string $effect
     * @param int $cost
     */
    public function __construct(int $damage, string $effect, int $cost)
    {
        parent::__construct("Fireball", $cost);
        $this->damage = $damage;
        $this->effect = $effect;
    }

    /**
     * @return int
     */
    public function getDamage(): int
    {
        return $this->damage;
    }

    /**
     * @param int $damage
     */
    public function setDamage(int $damage): void
    {
        $this->damage = $damage;
    }

    /**
     * @return string
     */
    public function getEffect(): string
    {
        return $this->effect;
    }

    /**
     * @param string $effect
     */
    public function setEffect(string $effect): void
    {
        $this->effect = $effect;
    }

    public function cast(character $caster, character $target): void
    {
        $targethp = $target->getHp();
        $target->setHp($targethp - $this->damage);
        echo $target->getName() . ' subit ' . $this->getDamage() . ' dommages. ' .
            'Point de vie restant : ' .$target->getHp() . ' ('. $targethp . ' - ' . $this->getDamage() . '). ';
    }
}