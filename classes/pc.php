<?php

namespace classes;

use classes\character;

class pc extends character
{

    public string $pseudo = "pseudo";

    /**
     * @return string
     */
    public function getPseudo(): string
    {
        return $this->pseudo;
    }

    /**
     * @param string $pseudo
     */
    public function setPseudo(string $pseudo): void
    {
        $this->pseudo = $pseudo;
    }


    //todo : ajoutez une propriété publique "pseudo"

}

