<?php

namespace classes;
/**
 * Que sont les espaces de noms ? Dans leur définition la plus large, ils représentent un moyen d'encapsuler des éléments.
 * Cela peut être conçu comme un concept abstrait, pour plusieurs raisons. Par exemple, dans un système de fichiers,
 * les dossiers représentent un groupe de fichiers associés et servent d'espace de noms pour les fichiers qu'ils contiennent.
 * Un exemple concret est que le fichier foo.txt peut exister dans les deux dossiers /home/greg et /home/other,
 * mais que les deux copies de foo.txt ne peuvent pas co-exister dans le même dossier.
 * De plus, pour accéder au fichier foo.txt depuis l'extérieur du dossier /home/greg,
 * il faut préciser le nom du dossier en utilisant un séparateur de dossier, tel que /home/greg/foo.txt.
 * Le même principe s'applique aux espaces de noms dans le monde de la programmation.
 *
 * Dans le monde PHP,
 * les espaces de noms sont conçus pour résoudre deux problèmes que rencontrent les auteurs de bibliothèques et d'applications lors de la réutilisation d'éléments tels que des classes ou des bibliothèques de fonctions :
 * - Collisions de noms entre le code que vous créez, les classes, fonctions ou constantes internes de PHP, ou celle de bibliothèques tierces.
 * - La capacité de faire des alias ou de raccourcir des Noms_Extremement_Long pour aider à la résolution du premier problème, et améliorer la lisibilité du code.
 *
 * Note: Les noms d'espaces de noms ne sont pas sensible à la casse.
 * Note: Les espaces de noms PHP (PHP\...) sont réservés pour l'utilisation interne du langage.
 */
class character
{

    public string $name = 'character';
    private int $id;
    private int $hp;
    private int $attack;
    private int $defense;
    private int $mana;
    private weapon $weapon;
    private race $race;
    private spell $magic;
    private ?Armor $helmet = null;
    private ?Armor $chestPlate = null;
    private ?Armor $gloves = null;
    private ?Armor $boots = null;
    private ?Armor $shield = null;

    /**
     * @param string $name
     * @param int $id
     */
    public function __construct(string $name, int $id, weapon $weapon, race $race, spell $magic)
    {
        $this->name = $name;
        $this->id = $id;
        $this->weapon = $weapon;
        $this->race = $race;
        $this->hp = $race->getHp();
        $this->attack = $race->getAttack();
        $this->defense = $race->getDefense();
        $this->mana = $race->getMana();
        $this->magic = $magic;
    }

    /**
     * @return int
     */
    public function getMana(): int
    {
        return $this->mana;
    }

    /**
     * @param int $mana
     */
    public function setMana(int $mana): void
    {
        $this->mana = $mana;
    }

    /**
     * @return spell
     */
    public function getMagic(): spell
    {
        return $this->magic;
    }

    /**
     * @param spell $magic
     */
    public function setMagic(spell $magic): void
    {
        $this->magic = $magic;
    }

    /**
     * GETTERS
     */

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getHp(): int
    {
        return $this->hp;
    }

    public function getAttack(): float
    {
        return $this->attack;
    }

    public function getDefense(): int
    {
        return $this->defense;
    }


    /**
     * SETTERS
     */

    /**
     * @param int $id
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setHp(int $hp): void
    {
        $this->hp = $hp;
    }

    public function setAttack(int $attack): void
    {
        $this->attack = $attack;
    }

    public function setDefense(int $defense): void
    {
        $this->defense = $defense;
    }

    /**
     * @return weapon
     */
    public function getWeapon(): weapon
    {
        return $this->weapon;
    }

    /**
     * @param weapon $weapon
     */
    public function setWeapon(weapon $weapon): void
    {
        $this->weapon = $weapon;
    }

    /**
     * @return race
     */
    public function getRace(): race
    {
        return $this->race;
    }

    /**
     * @param race $race
     */
    public function setRace(race $race): void
    {
        $this->race = $race;
    }

    public function WeaponRandDmg()
    {
        $result = rand($this->getWeapon()->getMindmg(), $this->getWeapon()->getMaxdmg());
        return $result;
    }

    /**
     * @param character $target
     * @return string
     */
    public function Attack(character $target): string
    {
        if (rand(1, 100) <= $target->getRace()->getDodge()) {
            return $target->getName() . ' esquive l\'attaque de ' .$this->getName();
        }
        $damage = $this->WeaponRandDmg();
        $defense = $target->getDefense();
        $finalDamage = ($damage + $this->attack) - ($defense);
        if ($finalDamage > 0) {
            $health = $target->getHp() - $finalDamage;
            $string = 'Touché ! ';
            if ($health > 0) {
                $string .= 'Point de vie restant : ' . $health . ' (' . $this->getHp() . ' - ' . $finalDamage . '). ';
            } else {
                $health = 0;
                $string .= 'mort!';
            }
            $target->setHp($health);
        } elseif ($finalDamage < 0) {
            $string = 'raté';
        } else {
            $string = 'paré';
        }
        $weapon = $this->getWeapon();
        return $this->name . ' attaque ' . $target->name . ' avec ' . $weapon->getName() . '. ' . $target->getName() . ' subit une attaque de puissance ' . $damage + $this->attack . ' (' . $damage . '+' . $this->attack . ') et la réduit de ' . $defense . ' grâce à son armure. <br>  <br> Résultat : ' . $finalDamage . '. ' . $string . '<br> ';

    }

    public function CastSpell(character $target): bool
    {
        $mana = $this->getMana();
        $spell = $this->getMagic();
        $cost = $spell->getCost();
        if ($mana >= $cost) {
            echo $this->getName() . ' lance ' . $spell->getName() . '. ';
            $spell->cast($this, $target);
            $this->setMana($mana - $cost);
            br();
            return true;
        } else {
            echo 'Mana manquante pour lancer ' . $spell->getName() . ' (' . $mana . '/' . $cost . '). ';
            return false;
        }
    }

    public function RegenerateMana(): void
    {
        $this->setMana(min($this->getMana() + $this->getRace()->getRegenMana(), $this->getRace()->getMana()));
    }

    public function equip(?Armor $armor): void
    {
        if ($armor == null) {
            return;
        }
        $armorType = $armor->getArmorType();
        switch ($armorType) {
            case ArmorType::Helmet:
                if ($this->helmet != null) {
                    $this->helmet->revertAugments($this);
                }
                $this->helmet = $armor;
                $armor->applyAugments($this);
                break;
            case ArmorType::ChestPlate:
                if ($this->chestPlate != null) {
                    $this->chestPlate->revertAugments($this);
                }
                $this->chestPlate = $armor;
                $armor->applyAugments($this);
                break;
            case ArmorType::Gloves:
                if ($this->gloves != null) {
                    $this->gloves->revertAugments($this);
                }
                $this->gloves = $armor;
                $armor->applyAugments($this);
                break;
            case ArmorType::Boots:
                if ($this->boots != null) {
                    $this->boots->revertAugments($this);
                }
                $this->boots = $armor;
                $armor->applyAugments($this);
                break;
            case ArmorType::Shield:
                if ($this->shield != null) {
                    $this->shield->revertAugments($this);
                }
                $this->shield = $armor;
                $armor->applyAugments($this);
                break;
            default:
                echo 'Cas non géré ';
        }
        //todo : Revert equip item before equipping new (vérifier si il est nul dans case avant de remplacer la valeur)
    }

    public function getHelmet(): ?Armor
    {
        return $this->helmet;
    }

    public function getChestPlate(): ?Armor
    {
        return $this->chestPlate;
    }

    public function getGloves(): ?Armor
    {
        return $this->gloves;
    }

    public function getBoots(): ?Armor
    {
        return $this->boots;
    }

    public function getShield(): ?Armor
    {
        return $this->shield;
    }

}