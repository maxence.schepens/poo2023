<?php

namespace classes;

class reinforcement extends spell
{

    public string $effect = "Physical Reinforcement";
    private int $reinforcementValue;

    /**
     * @param int $cost
     * @param string $effect
     */
    public function __construct(int $cost, string $effect, int $reinforcementValue)
    {
        parent::__construct("Reinforcement", $cost);
        $this->effect = $effect;
        $this->reinforcementValue = $reinforcementValue;
    }

    /**
     * @return string
     */
    public function getEffect(): string
    {
        return $this->effect;
    }

    /**
     * @param string $effect
     */
    public function setEffect(string $effect): void
    {
        $this->effect = $effect;
    }

    public function cast(character $caster, character $target): void
    {
        $defense = $caster->getDefense();
        $caster->setDefense($defense + $this->reinforcementValue);
        echo $caster->getName() . ' augmente sa défense de : ' . $this->reinforcementValue . ' et passe à ' . $caster->getDefense() . ' (' . $defense . ' + ' . $this->reinforcementValue . '). ';
    }

}