<?php

namespace classes;

enum ArmorType
{
case Helmet;
case ChestPlate;
case Gloves;
case Boots;
case Shield;
case Ring;
}
