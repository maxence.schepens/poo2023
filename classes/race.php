<?php

namespace classes;

class race
{
    public static race $human;
    public static race $elf;
    public static race $orc;

    public static function init()
    {

        race::$human = new race ("Human", 100, 15, 10,17, 5,  15, 15, 15 ,15 ,15);
        race::$elf = new race ("Elf", 80, 12, 10, 20, 7,  12, 15, 17, 17, 20);
        race::$orc = new race ("Orc", 120, 17, 14, 15, 3,  rand(17, 20), 18, 14, 14, 12);
    }

    private string $name = "Default";
    private int $hp = 100;
    private int $attack ;
    private int $defense;
    private int $mana = 20;
    private int $regenMana = 5;
    private int $block;
    private int $perfectBlock;
    private int $str;
    private int $end;
    private int $dex;
    private int $agi;
    private int $int;


    private function __construct(string $name, int $hp,int $attack, int $defense, int $mana, int $regenMana, int $str, int $end, int $dex, int $agi, int $int)
    {
        $this->name = $name;
        $this->hp = $hp;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->mana = $mana;
        $this->regenMana = $regenMana;
        $this->str = $str;
        $this->end = $end;
        $this->dex = $dex;
        $this->agi = $agi;
        $this->int = $int;
    }

    /**
     * @return int
     */
    public function getEnd(): int
    {
        return $this->end;
    }

    /**
     * @return int
     */
    public function getMana(): int
    {
        return $this->mana;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getHp(): int
    {
        return $this->hp * ($this->getEnd() * 0.10);
    }

    /**
     * @return int
     */
    public function getRegenMana(): int
    {
        return $this->regenMana;
    }

    /**
     * @return int
     */
    public function getAttack(): int
    {
        return intval($this->attack * (($this->getStr() * 8) / 100));
    }

    /**
     * @return int
     */
    public function getDefense(): int
    {
        return intval($this->defense * (($this->getEnd() * 8) / 100));
    }

    /**
     * @return int
     */
    public function getStr(): int
    {
        return $this->str;
    }

    /**
     * @return int
     */
    public function getDex(): int
    {
        return $this->dex;
    }

    /**
     * @return int
     */
    public function getAgi(): int
    {
        return $this->agi;
    }

    /**
     * @return int
     */
    public function getInt(): int
    {
        return $this->int;
    }

    /**
     * @return float
     */
    public function getDodge(): float
    {
        return $this->getDex();
    }

    /**
     * @return int
     */
    public function getBlock(): int
    {
        return $this->block;
    }

    /**
     * @return int
     */
    public function getPerfectBlock(): int
    {
        return $this->perfectBlock;
    }

}