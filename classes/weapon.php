<?php

namespace classes;

class weapon
{
    private static ?array $weaponShelf = null;
    public static weapon $sword;
    public static weapon $hammer;
    public static weapon $dagger;
    public static weapon $axe;
    public static weapon $bow;

    public static function init()
    {
        weapon::$weaponShelf = [weapon::$sword = new weapon ('Sword', 5, 8),
            weapon::$hammer = new weapon ('Hammer', 8, 12),
            weapon::$dagger = new weapon ('Dagger', 4, 7),
            weapon::$axe = new weapon ('Axe', 9, 13),
            weapon::$bow = new weapon ('Bow', 4, 12)];
    }

    private string $name;

    private int $mindmg;

    private int $maxdmg;

    private function __construct(string $name, int $mindmg, int $maxdmg)
    {
        $this->name = $name;
        $this->mindmg = $mindmg;
        $this->maxdmg = $maxdmg;
    }

    public static function getRandomWeapon(): weapon
    {
        $random = array_rand(self::$weaponShelf, 1);
        return weapon::$weaponShelf[$random];
    }

    /**
     * @return weapon
     */
    public static function getSword(): weapon
    {
        return self::$sword;
    }

    /**
     * @param weapon $slash
     */
    public static function setSword(weapon $slash): void
    {
        self::$sword = $slash;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @return int
     */
    public function getMindmg(): int
    {
        return $this->mindmg;
    }


    /**
     * @return int
     */
    public function getMaxdmg(): int
    {
        return $this->maxdmg;
    }
}