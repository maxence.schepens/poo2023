<?php

namespace classes;


abstract class spell
{

    private static ?array $grimoire = null;

    public static function init(): void
    {
        spell::$grimoire = [
            new fireball(15, "Burn", 12),
            new reinforcement(15, "Physical Reinforcement", rand(1, 3)),
        ];
    }

    public static function getRandomMagic(): spell
    {
        $random = array_rand(self::$grimoire, 1);
        return spell::$grimoire[$random];
    }

    /**
     * @return array|null
     */
    public static function getGrimoire(): ?array
    {
        return self::$grimoire;
    }


    private string $name;
    private int $cost;

    /**
     * @param string $name
     * @param int $cost
     */
    public function __construct(string $name, int $cost)
    {
        $this->name = $name;
        $this->cost = $cost;
    }

    public abstract function cast(character $caster,character $target): void;


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return $this->cost;
    }
}